# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class GithubTopTrendingsItem(scrapy.Item):
    # define the fields for your item here like:
    href = scrapy.Field()
    description = scrapy.Field()
    lang = scrapy.Field()
    new_stars = scrapy.Field()