# encoding=utf8
import sys
import os

import scrapy
from bs4 import BeautifulSoup
from enum import Enum
from ..items import GithubTopTrendingsItem
import re

reload(sys)
sys.setdefaultencoding('utf8')

sys.path.append(os.path.join(os.path.dirname(__file__), '../../github_top_trendings'))
from config import Lang as Lang


class GithubSpider(scrapy.Spider):
    name = 'github_trending'
    allowed_domains = ["google.com"]
    start_urls = [
        "https://github.com/trending/%s?since=daily" % lang.value for lang in Lang
    ]

    def parse(self, response):
        bsObj = BeautifulSoup(response.text, 'html.parser')
        repo_list = bsObj.find_all('ol', class_='repo-list')
        repo_item = repo_list[0].find_all('li')
        for t2 in repo_item:
            item = GithubTopTrendingsItem()
            href = t2.select('div > h3 > a')[0].get('href')
            description = t2.select('div > p')[0].text if t2.select('div > p') else ''
            lang = t2.select('div > span')[0].text
            star_text = t2.select('div > span')[-1].text

            item['href'] = 'https://github.com%s' % href
            item['description'] = description.strip()
            item['lang'] = lang.strip()
            item['new_stars'] = int(re.findall('\d+', star_text)[0])
            yield item