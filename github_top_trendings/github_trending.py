from airflow import DAG
from datetime import datetime,timedelta
from airflow.operators.bash_operator import BashOperator

default_args = {
    'owner': 'meiyougonglv',
    'depends_on_past': False,
    'start_date': datetime(2018, 7, 24),
    'email': ['meiyougonglv@alumni.sjtu.edu.cn'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG('github', default_args=default_args)

github_daily_trending_cmd = '''
cd /home/mygl/PycharmProjects/weekly_500/github_top_trendings && rm -rf /home/mygl/github_trending/{{ds_nodash}}.json && scrapy crawl github_trending -o /home/mygl/github_trending/{{ds_nodash}}.json
'''
github_daily_trending = BashOperator(
    task_id='github_daily_trending',
    bash_command=github_daily_trending_cmd,
    depends_on_past=True,
    dag=dag)

send_email_cmd = '''
cd /home/mygl/PycharmProjects/weekly_500/github_top_trendings && python send_trending_email.py -d {{ds_nodash}}
'''
send_email = BashOperator(
    task_id='send_email',
    bash_command=send_email_cmd,
    depends_on_past=True,
    dag=dag
)
send_email.set_upstream(github_daily_trending)


if __name__ == "__main__":
    dag.cli()
