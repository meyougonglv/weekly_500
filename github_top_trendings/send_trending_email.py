#!/usr/bin/python
# -*- coding: UTF-8 -*-
import os
import sys
import ConfigParser
import argparse
import json
import logging
import operator
import smtplib
from email.header import Header
from email.mime.text import MIMEText

from config import Lang as Lang
from pymongo import MongoClient
from datetime import datetime,timedelta

reload(sys)
sys.setdefaultencoding('utf-8')

DATA_DIR = '/home/mygl/github_trending/'
TOP_TRENDING_NUM = 1


def get_new_trending(date, force_send=False):
    yesterday = datetime.strftime(datetime.strptime(date, '%Y%m%d') - timedelta(days=1), '%Y%m%d')
    this_date_path = DATA_DIR + date + '.json'
    this_date_trending = json.load(open(this_date_path, 'r'))

    os.system('rm -rf %s' % (DATA_DIR + yesterday + '.json'))
    grouped_by_lan = dict()

    for lang in Lang:
        grouped_by_lan[lang.value] = []
    for item in this_date_trending:
        item['new_stars'] = int(item['new_stars'])
        grouped_by_lan[item['lang'].lower()].append(item)

    conn = MongoClient('localhost')
    db = conn.github_trending
    set = db.daily
    for group in grouped_by_lan.values():
        group = sorted(group, key=operator.itemgetter('new_stars'), reverse=True)
        if len(group) <= 0:
            continue
        group[0]['date'] = date
        set.update({"date": date, "lang": group[0]['lang']}, {"$set": group[0]}, upsert=True)

    yesterday_data = []
    for x in set.find({"date": yesterday}, {"_id": 0}):
        yesterday_data.append(x)
    today_data = []
    for x in set.find({"date": date}, {"_id": 0}):
        today_data.append(x)
    conn.close()
    if force_send:
        return today_data
    result = []
    if today_data != yesterday_data:
        result = today_data
    return result


def send_email(date, json_data):
    if not json_data or len(json_data) == 0:
        return
    session = 'smtp_config'
    config_parser = ConfigParser.ConfigParser()
    config_parser.read('credential.conf')
    user = config_parser.get(session, 'user')
    pwd = config_parser.get(session, 'pwd')
    host = config_parser.get(session, 'host')
    mail_receiver = config_parser.get(session, 'receivers')
    mail_sender = config_parser.get(session, 'sender')
    mail_host = host  # 设置服务器
    mail_user = user  # 用户名
    mail_pass = pwd  # 口令

    sender = mail_sender
    receivers = mail_receiver.split(',')

    html_items = []
    for item in json_data:
        single_span = '<h1>%s</h1><p>%s <a href="%s">%s</a> %d stars</p>' % (
        item['lang'], item['description'].decode('unicode_escape'), item['href'], item['href'], item['new_stars'])
        html_items.append(single_span)
    heml_content = '<html><body>%s%s%s</body></html>' % (html_items[0], html_items[1], html_items[2])
    message = MIMEText(heml_content, 'html', 'utf-8')

    message['Subject'] = '%s github trending 爬取通知' % date
    message['From'] = sender
    # message['To'] should be a str, multi-user should connect with ','
    message['To'] = mail_receiver

    subject = 'Trending Github Project %s' % date
    message['Subject'] = Header(subject, 'utf-8')

    try:
        smtpObj = smtplib.SMTP()
        smtpObj.connect(mail_host, 25)  # 25 为 SMTP 端口号
        smtpObj.ehlo()
        smtpObj.starttls()
        smtpObj.login(mail_user, mail_pass)
        smtpObj.sendmail(sender, receivers, message.as_string())
        logging.info("邮件发送成功")
        print("邮件发送成功")
        smtpObj.quit()
    except smtplib.SMTPException as e:
        logging.error("无法发送邮件 exception is %s" % e)
        print("无法发送邮件 exception is %s" % e.message)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='send email runner')
    parser.add_argument('-d', "--date", type=str, help='run date format is 20180728')
    parser.add_argument('--force', action='store_true', help='force send email ignore difference with his')
    args = parser.parse_args()
    new_trending = get_new_trending(args.date, args.force)
    send_email(args.date, new_trending)
