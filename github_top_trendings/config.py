from enum import Enum


class Lang(Enum):
    Java = 'java'
    Cpp = 'c++'
    Python = 'python'